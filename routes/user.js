
/*
 * GET users listing.
 */

exports.signin = function(req, res){
  res.render('signin');
};

exports.list = function(req, res){

	global.User.find({},function(err, u){
		
		if (err){
			console.log('Error al mostrar todos los usuarios');
		}
		else{
			console.log('Mostrando usuarios con exito');
			res.render('users.jade', { usuarios: u });
		}
	});
};

exports.login = function(req, res){

	global.User.findOne( { email : req.body.email } , function(err, u){
		if (!err && u) {
      // Found user register session
      req.session.correo = u.email;

  		console.log('Usuario: ' + u.email + ' Clave: ' + u.password);
      res.redirect('/dashboard');
    } else {
      // User not found lets go through login again
      res.redirect('/signin');
    }
	});
};

exports.signup = function(req, res){
	res.render('signup.jade');
};

exports.new = function(req, res){

	var email = req.body.email,
			password = req.body.password;


	var usuario_datos = {
		email: email,
		password: password
	};

	var user = new User(usuario_datos);

	user.save(function(err, data){
		if(err){
			console.log('Error al insertar usuario');
			res.redirect('/signup');
		}
		else{
			console.log('Usuario insertado');
			res.redirect('/signin');
		}
	});
};

exports.dashboard = function(req, res){
	console.log(req.session.correo);
	res.render('dashboard.jade', { correo : req.session.correo });
};
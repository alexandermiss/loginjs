/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , user = require('./routes/user')
  , http = require('http')
  , mongoose = require('mongoose')
  , path = require('path');

var app = express();

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

// ------------------------------------------------

var mongo;

app.configure('development', function(){
	mongo = {
		"hostname":"localhost",
		"port":27017,
		"username":"",
		"password":"",
		"name":"mongo-nagendajs",
		"db":"loginjs"
	}
});

app.configure('production', function(){
	var env = JSON.parse(process.env.VCAP_SERVICES);
	mongo = env['mongodb-1.8'][0]['credentials'];
});

var generate_mongo_url = function(obj){
	obj.hostname = (obj.hostname || 'localhost');
	obj.port = (obj.port || 27017);
	obj.db = (obj.db || 'test');

	if(obj.username && obj.password){
		return "mongodb://" + obj.username + ":" + obj.password + "@" + obj.hostname + ":" + obj.port + "/" + obj.db;
	}else{
		return "mongodb://" + obj.hostname + ":" + obj.port + "/" + obj.db;
	}
}

var mongourl = generate_mongo_url(mongo);

// all environments
app.set('port', process.env.PORT || 8080);
//app.set('port', process.env.VCAP_APP_PORT || 8080);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
// -----------------------------------
// Codigo creador por HadasaDevs
app.use(express.cookieParser());
app.use(express.session({ secret: '1234567890QWERTY'}));
// -----------------------------------
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

mongoose.connect(mongourl);

global.Schema = mongoose.Schema;
global.ObjectId = Schema.ObjectId;

global.User = new Schema({
	email: String,
	password: String
});

global.User = mongoose.model('User', User);

// -------------------------------------
app.get('/', routes.index);
app.get('/signin', user.signin);
app.get('/users', user.list);

app.post('/signin', user.login);

app.get('/signup', user.signup);

app.post('/user/new', user.new);

app.get('/dashboard', user.dashboard);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
